import React, { Component } from 'react';
import './App.css';

// utils
import setProp from './helpers/set-props';
import addition from './helpers/addition';

// UI
import Indicator from './components/indicator';
import NumberInput from './components/number-input';
import Button from './components/button';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firstNumber: 0,
      secondNumber: 0,
      sum: 0
    }
  }

  addition = () =>
    this.setState(addition)

  setNum = obj => e =>
    this.setState(setProp(obj)(e.target.value))

  render() {
    return (
      <div>
        <NumberInput 
          idName="firstNum"
          onChangeFn={ this.setNum }
          propName="firstNumber"
        />

        <NumberInput 
          idName="secondNum"
          onChangeFn={ this.setNum }
          propName="secondNumber"
        />

        <Button  
          label="Add"
          onClickFn={ this.addition }
        />

        <Indicator 
          sum={ this.state.sum }
        />

        <div id='sum'>
          { this.state.sum } 
        </div>
      </div>
    );
  }
}

export default App;
