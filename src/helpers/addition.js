export default ({
  firstNumber,
  secondNumber
}) => {
  let firstInt = parseInt(firstNumber)
  let secondInt = parseInt(secondNumber)

  return {
    sum: firstInt + secondInt
  }
}