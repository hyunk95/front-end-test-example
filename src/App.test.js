import React from 'react';
import { expect } from 'chai';
import Enzyme,{ mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';

import App from './App';
import Indicator from './components/indicator';
import NumberInput from './components/number-input';
import SomeButton from './components/button';

Enzyme.configure({ adapter: new Adapter() });

/**
 * Things you should test:
 *  - the existence of certain components / html elements within the application
 *    * WHY? depending on certain conditions we need to render different components
 *    * we need to check whether or not it is rendering the components we want
 *  - stub functions and check whether it is being called with the correct values
 *    * WHY? Is the function being invoked with the right values 
 *  - elements have the correct values
 *  - the actual functionalities of the functions dont need to be tested here - needed to be
 *    tested in a seperate file
 */

describe('<App />', () => {
  let app, indicator, numberInputs, button

  beforeEach(() => {
    app = mount(
      <App />
    )
  })

  beforeEach(() => {
    numberInputs = app.find(NumberInput)
  })

  it('renders', () => {
    expect(app).to.exist
  })

  describe('Indicator', () => {
    beforeEach(() => {
      indicator = app.find(Indicator)
    })

    it('should exist', () => {
      expect(indicator).to.exist
    })
  })

  it('should have two number inputs', () => {
    expect(numberInputs).to.have.lengthOf(2)
  })

  describe('First Number Input', () => {
    let firstInput

    beforeEach(() => {
      firstInput = app.find('#firstNum')
    })

    it('should exist', () => {
      expect(firstInput).to.exist
    })

    it('should have id', () => {
      expect(firstInput.props().id).to.eql("firstNum")
    })
  })

  describe('Second Number Input', () => {
    let secondInput

    beforeEach(() => {
      secondInput = app.find('#secondNum')
    })

    it('should exist', () => {
      expect(secondInput).to.exist
    })

    it('should have id', () => {
      expect(secondInput.props().id).to.eql("secondNum")
    })
  })

  describe('Button', () => {
    beforeEach(() => {
      button = app.find(SomeButton)
    })

    it('should exist', () => {
      expect(button).to.exist
    })

    it('should have label', () => {
      expect(button.props().label).to.eql("Add")
    })
  })
})