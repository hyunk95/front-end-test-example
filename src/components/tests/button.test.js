import React from 'react';
import { expect } from 'chai';
import Enzyme,{ mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';

import Button from '../button'

Enzyme.configure({ adapter: new Adapter() });

describe('<Button />', () => {
  let buttonSpy, button, buttonInstance

  beforeEach(() => {
    buttonSpy = sinon.spy();
    button = mount(<Button label='some label' onClickFn={ buttonSpy } />)
    button.simulate('click')
  })

  beforeEach(() => {
    buttonInstance = button.find('button')
  })

  it('should have calledOnce', () => {
    expect(buttonSpy).to.have.property('callCount', 1);
  })

  it('should have text which is the prop label', () => {
    expect(buttonInstance.text()).to.eql('some label')
  })
})