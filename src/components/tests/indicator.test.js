import React from 'react';
import { expect } from 'chai';
import Enzyme,{ mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';

import Indicator from '../indicator'

Enzyme.configure({ adapter: new Adapter() });

describe('<Indicator />', () => {
  let indicator
  describe('when sum is even', () => {
    beforeEach(() => {
      indicator = mount(<Indicator sum={ 2 } />)
    })

    it('should have text EVEN', () => {
      expect(indicator.text()).to.eql("EVEN")
    })
  })

  describe('when sum is odd', () => {
    beforeEach(() => {
      indicator = mount(<Indicator sum={ 3 } />)
    })

    it('should have text ODD', () => {
      expect(indicator.text()).to.eql("ODD")
    })
  })

  // what to do if 0 ????
})