import React from 'react';

const NumberInput = ({
  idName,
  onChangeFn,
  propName
}) =>
  <input id={idName} type="text" onChange={ onChangeFn(propName) } />

export default NumberInput