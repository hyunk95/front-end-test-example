import React from 'react'

const Indicator = ({ sum }) =>
    sum % 2 === 0 

      ? <div className="indicator">EVEN</div>

      : <div className="indicator">ODD</div>

export default Indicator