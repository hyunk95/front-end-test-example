import React from 'react';

const SomeButton = ({
  label,
  onClickFn
}) =>
  <button onClick={onClickFn} >{ label }</button>

export default SomeButton